//
//  PoporNetRecordAppDelegate.h
//  PoporNetRecord
//
//  Created by popor on 07/06/2018.
//  Copyright (c) 2018 popor. All rights reserved.
//

@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
