//
//  PoporAFNConfig.h
//  PoporAFN
//
//  Created by popor on 17/4/28.
//  Copyright © 2017年 popor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "PoporAfnEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface PoporAfnConfig : NSObject

// 0: block宏
typedef void(^PoporAfnEntityBlock)(PoporAfnEntity * pae);
typedef AFHTTPSessionManager * _Nullable (^PoporAfnSMBlock)(void);
typedef NSDictionary         * _Nullable (^PoporAfnHeaderBlock)(void);

// 1: APP 需要设置p默认的head block. 假如需要设置单独的head可以自定义,使用PoporAFN的自定义manage接口, 尽量不要这里设置header了.
@property (nonatomic, copy  ) PoporAfnSMBlock afnSMBlock;

// 2: header需要单独设置了, 不然在处理post image 时候会出错.
@property (nonatomic, copy  ) PoporAfnHeaderBlock afnHeaderBlock;

// 3: 记录用block
@property (nonatomic, copy  ) PoporAfnEntityBlock recordBlock;

// 4: 额外的全局回调, 比如追踪登录权限异常
@property (nonatomic, copy  ) PoporAFNBlock_PDicPae   extraSuccessBlock;
@property (nonatomic, copy  ) PoporAFNBlock_PErrorPae extraFailBlock;

+ (PoporAfnConfig *)share;

// 设置manger,主要用于自定义head.
+ (AFHTTPSessionManager *)createManager;

// 默认head
+ (NSDictionary *)createHeader;

#pragma mark - manange
+ (AFURLSessionManager *)postFormDataManager;

+ (AFURLSessionManager *)postXformManager;

+ (AFURLSessionManager *)jsonManager;

#pragma mark -
+ (NSString *)titleOfUrl:(NSString *)url;

@end

NS_ASSUME_NONNULL_END
