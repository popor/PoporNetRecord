//
//  PoporAFNEntity.h
//  PoporAFN
//
//  Created by popor on 17/4/28.
//  Copyright © 2017年 popor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "PoporAfnCache.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, PoporMethod) {
    PoporMethodGet,
    PoporMethodPostJson,     // json格式的请求, 还需要配合manger 类型
    PoporMethodPostFormData, // 对应 PostMan 的 post 的 form-data,一般用于上传文件.
    PoporMethodPostXform,    // 对应 PostMan 的 post 的 x-www-form-urlencoded.
    PoporMethodPut,          // put, 修改部分数据
};

@class PoporAFN;
@class PoporAfnEntity;

typedef void(^PoporAFNBlock_PPostFormData)(id <AFMultipartFormData> formData);
typedef BOOL(^PoporAFNBlock_PPaeRBool)(PoporAfnEntity * pae);
typedef void(^PoporAFNBlock_PNSProgress)(NSProgress * uploadProgress);


typedef void(^PoporAFNBlock_PDicPae)(NSDictionary * dic, PoporAfnEntity * _Nonnull pae);
typedef void(^PoporAFNBlock_PErrorPae)(NSError * error, PoporAfnEntity * _Nonnull pae);

typedef void(^PoporAFNBlock_PAfn)(PoporAFN * afn);

//----------------------------------------------------------
@interface PoporAfnEntity : NSObject
// 参数
@property (nonatomic, copy  ) NSString             * title;
@property (nonatomic        ) PoporMethod            method;
@property (nonatomic, copy  ) NSString             * url;
@property (nonatomic, copy  ) id                     head;
@property (nonatomic, copy  ) id                     parameters;
@property (nonatomic, copy  ) id                     responseObject;

@property (nonatomic, copy  ) NSURLSessionDataTask * task;

@property (nonatomic        ) BOOL           success;// 是否成功, 因为遇到了 oss 即使成功, responseObject 也为空的情况.

@property (nonatomic        ) PoporAfnCacheType cacheType;
@property (nonatomic, copy  ) NSString        * cacheKey;
@property (nonatomic, copy  ) NSArray         * ignoreCacheArray;
@property (nonatomic        ) BOOL isCacheInfoBlocked; // 缓存数据是否已回调.

@property (nonatomic        ) BOOL isCached; // 是否是缓存数据.

@property (nonatomic, copy  ) NSDictionary * _Nullable dic; // 目前只处理dic类型.

@property (nonatomic, copy  ) NSError      * _Nullable error;

// 目前 cache 只保留 dic。
- (NSString *)cacheText;

@end

NS_ASSUME_NONNULL_END
