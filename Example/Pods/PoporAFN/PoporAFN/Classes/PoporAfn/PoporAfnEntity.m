//
//  PoporAFNEntity.m
//  PoporAFN
//
//  Created by popor on 17/4/28.
//  Copyright © 2017年 popor. All rights reserved.
//

#import "PoporAfnEntity.h"

@implementation PoporAfnEntity

- (NSDictionary *)dic {
    if (!_dic) {
        if ([_responseObject isKindOfClass:[NSDictionary class]]) {
            _dic = (NSDictionary *)_responseObject;
        } else if ([_responseObject isKindOfClass:[NSData class]]) {
            _dic = [NSJSONSerialization JSONObjectWithData:_responseObject options:NSJSONReadingAllowFragments error:nil];
        } else {
            _dic = nil;
        }
    }
    return _dic;
}

// 目前 cache 只保留 dic。
- (NSString *)cacheText {
    if (self.dic) {
        NSMutableDictionary * dicNet = nil;
        if (self.ignoreCacheArray.count > 0) {
            dicNet = [self.dic mutableCopy];
            for (NSString * ignoreText in self.ignoreCacheArray) {
                if (ignoreText) {
                    [dicNet removeObjectForKey:ignoreText];
                }
            }
        }
        
        NSDictionary * dic  = @{@"dic": dicNet ? :self.dic};
        
        NSData   * jsonData = [NSJSONSerialization dataWithJSONObject:dic options:kNilOptions error:nil];
        NSString * paraText = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        return paraText;
    } else {
        return @"";
    }
}

@end

