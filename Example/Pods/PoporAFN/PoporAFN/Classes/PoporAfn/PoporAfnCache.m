//
//  PoporAfnCache.m
//  PoporAFN
//
//  Created by popor on 17/4/28.
//  Copyright © 2017年 popor. All rights reserved.
//

#import "PoporAfnCache.h"

static NSString * kCacheFolder = @"PoporAfnCache";

@interface PoporAfnCache ()

@property (nonatomic, copy  ) NSString * simulatorCachePath;

@end

@implementation PoporAfnCache

+ (void)load {
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        [self createCachesFolder];
    });
}

+ (NSDictionary *)dicOfKey:(NSString * _Nullable)key
{
    if (!key) {
        return nil;
    }
    
    NSString * diskText = [self readKey:key];
    if (diskText) {
        NSData * data      = [diskText dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        
        return dic;
    } else {
        return nil;
    }
}


+ (BOOL)needSaveKey:(NSString * _Nullable)key text:(NSString * _Nullable)text
{
    if (!text || !key) {
        return NO;
    }
    
    NSString * diskText = [self readKey:key];
    if ([text isEqualToString:diskText]) {
        // 假如一致, 也要更新文件修改时间.
        [[NSFileManager defaultManager] setAttributes:@{NSFileModificationDate: [NSDate date]} ofItemAtPath:[self keyPath:key] error:nil];
        return NO;
    } else {
        [self saveKey:key value:text];
        return YES;
    }
}

+ (void)saveKey:(NSString *)key value:(NSString *)value {
    //    NSError * error;
    //    [value writeToFile:[self keyPath:key] atomically:YES encoding:NSUTF8StringEncoding error:&error];
    //
    //    NSLog(@"error: %@", error.localizedDescription);
    
    [value writeToFile:[self keyPath:key] atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

// 现在只负责读, 不负责判断文件是否超时.
+ (NSString *)readKey:(NSString *)key {
    //NSString * path = [[NSBundle mainBundle] pathForResource:@"liveCity" ofType:@"json"];
    //NSData   * data = [NSData dataWithContentsOfFile:path];
    
    NSString * filePath = [self keyPath:key];
    NSData   * data = [NSData dataWithContentsOfFile:filePath];
    NSString * str  = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return str;
}

+ (NSTimeInterval)cacheTimeStampWithKey:(NSString *)key {
    NSString * filePath = [self keyPath:key];
    NSDate   * modeDate = [self fileDate:filePath];
    if (modeDate)
        return modeDate.timeIntervalSince1970;
    return 0;
}

+ (NSString *)keyPath:(NSString *)key {
    NSString * path = [NSString stringWithFormat:@"%@/%@.txt", [self basePath], key];
    return path;
}

+ (void)createCachesFolder {
    [[NSFileManager defaultManager] createDirectoryAtPath:[self basePath] withIntermediateDirectories:YES attributes:nil error:nil];
}

+ (instancetype)share {
    static dispatch_once_t once;
    static id instance;
    dispatch_once(&once, ^{
        instance = [self new];
        
    });
    return instance;
}

// 请尽量在开发环境下使用.
+ (void)setSimulatorCachePath:(NSString * _Nullable)simulatorCachePath {
#if TARGET_IPHONE_SIMULATOR//模拟器
    PoporAfnCache * share = [PoporAfnCache share];
    if ([simulatorCachePath hasSuffix:@"/"]) {
        simulatorCachePath = [simulatorCachePath substringToIndex:simulatorCachePath.length -1];
    }
    share.simulatorCachePath = simulatorCachePath;
    NSLog(@"‼️ PoporAfnCache custome simulatorCachePath: %@ ‼️", simulatorCachePath);
    
    if (simulatorCachePath.length > 5) {
        [[NSFileManager defaultManager] createDirectoryAtPath:simulatorCachePath
                                  withIntermediateDirectories:YES attributes:nil error:nil];
    }
#elif TARGET_OS_IPHONE//真机
    
#endif
    
}

+ (NSString *)simulatorCachePath {
    PoporAfnCache * share = [PoporAfnCache share];
    return share.simulatorCachePath;
}

+ (NSString *)basePath {
#if TARGET_IPHONE_SIMULATOR//模拟器
    PoporAfnCache * share = [PoporAfnCache share];
    if (share.simulatorCachePath) {
        return share.simulatorCachePath;
    }
    
#elif TARGET_OS_IPHONE//真机
    
#endif
    
    static NSString * path;
    if (!path) {
        NSArray  * pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSMutableString * cachPath = [[NSMutableString alloc] initWithString:[pathsToDocuments objectAtIndex:0]];
        [cachPath setString:[cachPath stringByReplacingOccurrencesOfString:@"/Documents" withString:@"/Library/Caches"]];
        
        path = [NSString stringWithFormat:@"%@/%@", cachPath, kCacheFolder];
    }
    return path;
}

+ (NSDate *)fileDate:(NSString *)path {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //NSDictionary *fileAttributes = [fileManager fileAttributesAtPath:path traverseLink:YES];
    //NSLog(@"@@");
    
    NSError * error;
    NSDictionary *fileAttributes = [fileManager attributesOfItemAtPath:path error:&error];
    if (error) {
        return nil;
    }
    
    if (fileAttributes != nil) {
        //        NSNumber *fileSize;
        //        NSString *fileOwner, *creationDate;
        //        NSDate *fileModDate;
        //NSString *NSFileCreationDate
        //文件大小
        //        if (fileSize = [fileAttributes objectForKey:NSFileSize]) {
        //            NSLog(@"File size: %qi\n", [fileSize unsignedLongLongValue]);
        //        }
        //
        //        //文件创建日期
        //        if (creationDate = [fileAttributes objectForKey:NSFileCreationDate]) {
        //            NSLog(@"File creationDate: %@\n", creationDate);
        //            //textField.text=NSFileCreationDate;
        //        }
        //
        //        //文件所有者
        //        if (fileOwner = [fileAttributes objectForKey:NSFileOwnerAccountName]) {
        //            NSLog(@"Owner: %@\n", fileOwner);
        //        }
        //
        //        //文件修改日期
        //        if (fileModDate = [fileAttributes objectForKey:NSFileModificationDate]) {
        //            NSLog(@"Modification date: %@\n", fileModDate);
        //        }
        
        NSDate *fileModDate = [fileAttributes objectForKey:NSFileModificationDate];
        return fileModDate;
    }
    else {
        //NSLog(@"Path (%@) is invalid.", path);
        return nil;
    }
}


#pragma mark - 清空缓存数据
+ (void)clearCacheData {
    [[NSFileManager defaultManager] removeItemAtPath:[self basePath] error:nil];
    [self createCachesFolder];
}

// 假如发现缓存数据需要更新的话
+ (void)removeUrl:(NSString * _Nullable)url {
    if (!url) {
        return;
    }
    
    NSString * filePath = [self keyPath:url];
    
    if (!filePath) {
        return;
    }else{
        filePath = [filePath stringByRemovingPercentEncoding];
    }
    
    NSError *error;
    if ([[NSFileManager defaultManager] removeItemAtPath:filePath error:&error] != YES) {
        NSLog(@"Unable to delete file: %@", [error localizedDescription]);
    }
}


@end
