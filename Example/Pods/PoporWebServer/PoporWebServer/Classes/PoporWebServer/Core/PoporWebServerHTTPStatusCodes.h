/*
 Copyright (c) 2012-2019, Pierre-Olivier Latour
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * The name of Pierre-Olivier Latour may not be used to endorse
 or promote products derived from this software without specific
 prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL PIERRE-OLIVIER LATOUR BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
// http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml

#import <Foundation/Foundation.h>

/**
 *  Convenience constants for "informational" HTTP status codes.
 */
typedef NS_ENUM(NSInteger, PoporWebServerInformationalHTTPStatusCode) {
  kPoporWebServerHTTPStatusCode_Continue = 100,
  kPoporWebServerHTTPStatusCode_SwitchingProtocols = 101,
  kPoporWebServerHTTPStatusCode_Processing = 102
};

/**
 *  Convenience constants for "successful" HTTP status codes.
 */
typedef NS_ENUM(NSInteger, PoporWebServerSuccessfulHTTPStatusCode) {
  kPoporWebServerHTTPStatusCode_OK = 200,
  kPoporWebServerHTTPStatusCode_Created = 201,
  kPoporWebServerHTTPStatusCode_Accepted = 202,
  kPoporWebServerHTTPStatusCode_NonAuthoritativeInformation = 203,
  kPoporWebServerHTTPStatusCode_NoContent = 204,
  kPoporWebServerHTTPStatusCode_ResetContent = 205,
  kPoporWebServerHTTPStatusCode_PartialContent = 206,
  kPoporWebServerHTTPStatusCode_MultiStatus = 207,
  kPoporWebServerHTTPStatusCode_AlreadyReported = 208
};

/**
 *  Convenience constants for "redirection" HTTP status codes.
 */
typedef NS_ENUM(NSInteger, PoporWebServerRedirectionHTTPStatusCode) {
  kPoporWebServerHTTPStatusCode_MultipleChoices = 300,
  kPoporWebServerHTTPStatusCode_MovedPermanently = 301,
  kPoporWebServerHTTPStatusCode_Found = 302,
  kPoporWebServerHTTPStatusCode_SeeOther = 303,
  kPoporWebServerHTTPStatusCode_NotModified = 304,
  kPoporWebServerHTTPStatusCode_UseProxy = 305,
  kPoporWebServerHTTPStatusCode_TemporaryRedirect = 307,
  kPoporWebServerHTTPStatusCode_PermanentRedirect = 308
};

/**
 *  Convenience constants for "client error" HTTP status codes.
 */
typedef NS_ENUM(NSInteger, PoporWebServerClientErrorHTTPStatusCode) {
  kPoporWebServerHTTPStatusCode_BadRequest = 400,
  kPoporWebServerHTTPStatusCode_Unauthorized = 401,
  kPoporWebServerHTTPStatusCode_PaymentRequired = 402,
  kPoporWebServerHTTPStatusCode_Forbidden = 403,
  kPoporWebServerHTTPStatusCode_NotFound = 404,
  kPoporWebServerHTTPStatusCode_MethodNotAllowed = 405,
  kPoporWebServerHTTPStatusCode_NotAcceptable = 406,
  kPoporWebServerHTTPStatusCode_ProxyAuthenticationRequired = 407,
  kPoporWebServerHTTPStatusCode_RequestTimeout = 408,
  kPoporWebServerHTTPStatusCode_Conflict = 409,
  kPoporWebServerHTTPStatusCode_Gone = 410,
  kPoporWebServerHTTPStatusCode_LengthRequired = 411,
  kPoporWebServerHTTPStatusCode_PreconditionFailed = 412,
  kPoporWebServerHTTPStatusCode_RequestEntityTooLarge = 413,
  kPoporWebServerHTTPStatusCode_RequestURITooLong = 414,
  kPoporWebServerHTTPStatusCode_UnsupportedMediaType = 415,
  kPoporWebServerHTTPStatusCode_RequestedRangeNotSatisfiable = 416,
  kPoporWebServerHTTPStatusCode_ExpectationFailed = 417,
  kPoporWebServerHTTPStatusCode_UnprocessableEntity = 422,
  kPoporWebServerHTTPStatusCode_Locked = 423,
  kPoporWebServerHTTPStatusCode_FailedDependency = 424,
  kPoporWebServerHTTPStatusCode_UpgradeRequired = 426,
  kPoporWebServerHTTPStatusCode_PreconditionRequired = 428,
  kPoporWebServerHTTPStatusCode_TooManyRequests = 429,
  kPoporWebServerHTTPStatusCode_RequestHeaderFieldsTooLarge = 431
};

/**
 *  Convenience constants for "server error" HTTP status codes.
 */
typedef NS_ENUM(NSInteger, PoporWebServerServerErrorHTTPStatusCode) {
  kPoporWebServerHTTPStatusCode_InternalServerError = 500,
  kPoporWebServerHTTPStatusCode_NotImplemented = 501,
  kPoporWebServerHTTPStatusCode_BadGateway = 502,
  kPoporWebServerHTTPStatusCode_ServiceUnavailable = 503,
  kPoporWebServerHTTPStatusCode_GatewayTimeout = 504,
  kPoporWebServerHTTPStatusCode_HTTPVersionNotSupported = 505,
  kPoporWebServerHTTPStatusCode_InsufficientStorage = 507,
  kPoporWebServerHTTPStatusCode_LoopDetected = 508,
  kPoporWebServerHTTPStatusCode_NotExtended = 510,
  kPoporWebServerHTTPStatusCode_NetworkAuthenticationRequired = 511
};
