/*
 Copyright (c) 2012-2019, Pierre-Olivier Latour
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * The name of Pierre-Olivier Latour may not be used to endorse
 or promote products derived from this software without specific
 prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL PIERRE-OLIVIER LATOUR BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import <os/object.h>
#import <sys/socket.h>

/**
 *  All PoporWebServer headers.
 */

#import "PoporWebServerHTTPStatusCodes.h"
#import "PoporWebServerFunctions.h"

#import "PoporWebServer.h"
#import "PoporWebServerConnection.h"

#import "PoporWebServerDataRequest.h"
#import "PoporWebServerFileRequest.h"
#import "PoporWebServerMultiPartFormRequest.h"
#import "PoporWebServerURLEncodedFormRequest.h"

#import "PoporWebServerDataResponse.h"
#import "PoporWebServerErrorResponse.h"
#import "PoporWebServerFileResponse.h"
#import "PoporWebServerStreamedResponse.h"

/**
 *  Check if a custom logging facility should be used instead.
 */

#if defined(__PoporWebServer_LOGGING_HEADER__)

#define __PoporWebServer_LOGGING_FACILITY_CUSTOM__

#import __PoporWebServer_LOGGING_HEADER__

/**
 *  Automatically detect if XLFacility is available and if so use it as a
 *  logging facility.
 */

#elif defined(__has_include) && __has_include("XLFacilityMacros.h")

#define __PoporWebServer_LOGGING_FACILITY_XLFACILITY__

#undef PXLOG_TAG
#define PXLOG_TAG @"PoporWebServer.internal"

#import "XLFacilityMacros.h"

#define PWS_LOG_DEBUG(...)   PXLOG_DEBUG(__VA_ARGS__)
#define PWS_LOG_VERBOSE(...) PXLOG_VERBOSE(__VA_ARGS__)
#define PWS_LOG_INFO(...)    PXLOG_INFO(__VA_ARGS__)
#define PWS_LOG_WARNING(...) PXLOG_WARNING(__VA_ARGS__)
#define PWS_LOG_ERROR(...)   PXLOG_ERROR(__VA_ARGS__)

#define PWS_DCHECK(__CONDITION__) PXLOG_DEBUG_CHECK(__CONDITION__)
#define PWS_DNOT_REACHED()        PXLOG_DEBUG_UNREACHABLE()

/**
 *  If all of the above fail, then use PoporWebServer built-in
 *  logging facility.
 */

#else

#define __PoporWebServer_LOGGING_FACILITY_BUILTIN__

typedef NS_ENUM(int, PoporWebServerLoggingLevel) {
  kPoporWebServerLoggingLevel_Debug = 0,
  kPoporWebServerLoggingLevel_Verbose,
  kPoporWebServerLoggingLevel_Info,
  kPoporWebServerLoggingLevel_Warning,
  kPoporWebServerLoggingLevel_Error
};

extern PoporWebServerLoggingLevel PoporWebServerLogLevel;
extern void PoporWebServerLogMessage(PoporWebServerLoggingLevel level, NSString* _Nonnull format, ...) NS_FORMAT_FUNCTION(2, 3);

#if DEBUG
#define PWS_LOG_DEBUG(...)                                                                                                             \
  do {                                                                                                                                 \
    if (PoporWebServerLogLevel <= kPoporWebServerLoggingLevel_Debug) PoporWebServerLogMessage(kPoporWebServerLoggingLevel_Debug, __VA_ARGS__); \
  } while (0)
#else
#define PWS_LOG_DEBUG(...)
#endif
#define PWS_LOG_VERBOSE(...)                                                                                                               \
  do {                                                                                                                                     \
    if (PoporWebServerLogLevel <= kPoporWebServerLoggingLevel_Verbose) PoporWebServerLogMessage(kPoporWebServerLoggingLevel_Verbose, __VA_ARGS__); \
  } while (0)
#define PWS_LOG_INFO(...)                                                                                                            \
  do {                                                                                                                               \
    if (PoporWebServerLogLevel <= kPoporWebServerLoggingLevel_Info) PoporWebServerLogMessage(kPoporWebServerLoggingLevel_Info, __VA_ARGS__); \
  } while (0)
#define PWS_LOG_WARNING(...)                                                                                                               \
  do {                                                                                                                                     \
    if (PoporWebServerLogLevel <= kPoporWebServerLoggingLevel_Warning) PoporWebServerLogMessage(kPoporWebServerLoggingLevel_Warning, __VA_ARGS__); \
  } while (0)
#define PWS_LOG_ERROR(...)                                                                                                             \
  do {                                                                                                                                 \
    if (PoporWebServerLogLevel <= kPoporWebServerLoggingLevel_Error) PoporWebServerLogMessage(kPoporWebServerLoggingLevel_Error, __VA_ARGS__); \
  } while (0)

#endif

/**
 *  Consistency check macros used when building Debug only.
 */

#if !defined(PWS_DCHECK) || !defined(PWS_DNOT_REACHED)

#if DEBUG

#define PWS_DCHECK(__CONDITION__) \
  do {                            \
    if (!(__CONDITION__)) {       \
      abort();                    \
    }                             \
  } while (0)
#define PWS_DNOT_REACHED() abort()

#else

#define PWS_DCHECK(__CONDITION__)
#define PWS_DNOT_REACHED()

#endif

#endif

NS_ASSUME_NONNULL_BEGIN

/**
 *  PoporWebServer internal constants and APIs.
 */

#define kPoporWebServerDefaultMimeType @"application/octet-stream"
#define kPoporWebServerErrorDomain @"PoporWebServerErrorDomain"

static inline BOOL PoporWebServerIsValidByteRange(NSRange range) {
  return ((range.location != NSUIntegerMax) || (range.length > 0));
}

static inline NSError* PoporWebServerMakePosixError(int code) {
  return [NSError errorWithDomain:NSPOSIXErrorDomain code:code userInfo:@{NSLocalizedDescriptionKey : (NSString*)[NSString stringWithUTF8String:strerror(code)]}];
}

extern void PoporWebServerInitializeFunctions(void);
extern NSString* _Nullable PoporWebServerNormalizeHeaderValue(NSString* _Nullable value);
extern NSString* _Nullable PoporWebServerTruncateHeaderValue(NSString* _Nullable value);
extern NSString* _Nullable PoporWebServerExtractHeaderValueParameter(NSString* _Nullable value, NSString* attribute);
extern NSStringEncoding PoporWebServerStringEncodingFromCharset(NSString* charset);
extern BOOL PoporWebServerIsTextContentType(NSString* type);
extern NSString* PoporWebServerDescribeData(NSData* data, NSString* contentType);
extern NSString* PoporWebServerComputeMD5Digest(NSString* format, ...) NS_FORMAT_FUNCTION(1, 2);
extern NSString* PoporWebServerStringFromSockAddr(const struct sockaddr* addr, BOOL includeService);

@interface PoporWebServerConnection ()
- (instancetype)initWithServer:(PoporWebServer*)server localAddress:(NSData*)localAddress remoteAddress:(NSData*)remoteAddress socket:(CFSocketNativeHandle)socket;
@end

@interface PoporWebServer ()
@property(nonatomic, readonly) NSMutableArray<PoporWebServerHandler*>* handlers;
@property(nonatomic, readonly, nullable) NSString* serverName;
@property(nonatomic, readonly, nullable) NSString* authenticationRealm;
@property(nonatomic, readonly, nullable) NSMutableDictionary<NSString*, NSString*>* authenticationBasicAccounts;
@property(nonatomic, readonly, nullable) NSMutableDictionary<NSString*, NSString*>* authenticationDigestAccounts;
@property(nonatomic, readonly) BOOL shouldAutomaticallyMapHEADToGET;
@property(nonatomic, readonly) dispatch_queue_priority_t dispatchQueuePriority;
- (void)willStartConnection:(PoporWebServerConnection*)connection;
- (void)didEndConnection:(PoporWebServerConnection*)connection;
@end

@interface PoporWebServerHandler : NSObject
@property(nonatomic, readonly) PoporWebServerMatchBlock matchBlock;
@property(nonatomic, readonly) PoporWebServerAsyncProcessBlock asyncProcessBlock;
@end

@interface PoporWebServerRequest ()
@property(nonatomic, readonly) BOOL usesChunkedTransferEncoding;
@property(nonatomic) NSData* localAddressData;
@property(nonatomic) NSData* remoteAddressData;
- (void)prepareForWriting;
- (BOOL)performOpen:(NSError**)error;
- (BOOL)performWriteData:(NSData*)data error:(NSError**)error;
- (BOOL)performClose:(NSError**)error;
- (void)setAttribute:(nullable id)attribute forKey:(NSString*)key;
@end

@interface PoporWebServerResponse ()
@property(nonatomic, readonly) NSDictionary<NSString*, NSString*>* additionalHeaders;
@property(nonatomic, readonly) BOOL usesChunkedTransferEncoding;
- (void)prepareForReading;
- (BOOL)performOpen:(NSError**)error;
- (void)performReadDataWithCompletion:(PoporWebServerBodyReaderCompletionBlock)block;
- (void)performClose;
@end

NS_ASSUME_NONNULL_END
