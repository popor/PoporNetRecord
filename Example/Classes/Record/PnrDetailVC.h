//
//  PnrDetailVC.h
//  PoporNetRecord
//
//  Created by popor on 2018/5/16.
//  Copyright © 2018年 popor. All rights reserved.

#import <UIKit/UIKit.h>
#import "PnrEntity.h"
#import "PnrConfig.h"

@interface PnrDetailVC : UIViewController

// self   : 自己的
@property (nonatomic, strong) UITableView * infoTV;
@property (nonatomic        ) int selectRow;
@property (nonatomic, strong) UIMenuController * menu;

// inject : 外部注入的
@property (nonatomic, strong) NSArray * jsonArray;
@property (nonatomic, strong) NSArray * titleArray;
@property (nonatomic, strong) NSArray * cellAttArray;
@property (nonatomic, copy  ) PnrBlockPPnrEntity blockExtraRecord; // 转发完成之后的回调
@property (nonatomic, weak  ) PnrEntity * weakPnrEntity;

- (instancetype)initWithDic:(NSDictionary *)dic;


@end
