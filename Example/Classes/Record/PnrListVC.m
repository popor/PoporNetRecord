
//
//  PnrListVC.m
//  PoporNetRecord
//
//  Created by popor on 2018/5/16.
//  Copyright © 2018年 popor. All rights reserved.

#import "PnrListVC.h"

#import "PnrConfig.h"
#import "PnrUITool.h"

#import "PnrWebServer.h"
#import "PnrExtraEntity.h"

#import "PnrDetailVC.h"
#import "PnrSetVC.h"
#import "PnrMessageVC.h"
#import "PoporNetRecord.h"


static int PnrListCellTextLeftGap  = 15;
static int PnrListCellTextRightGap = 35;

@interface PnrListVCCell : UITableViewCell

@property (nonatomic, strong) UILabel * requestL;
@property (nonatomic, strong) UILabel * domainL;

@property (nonatomic, strong) UILabel * timeL;

+ (int)cellLogH:(NSString *)log;

@end


@interface PnrListVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak  ) PnrConfig * config;
@property (nonatomic, weak  ) PnrExtraEntity * extraEntity;

@property (nonatomic, weak  ) PnrListVCCell * lastSelectCell;
@property (nonatomic, strong) UIImageView   * viewImageView;
@property (nonatomic, strong) UILabel       * setL;
@property (nonatomic        ) BOOL            dragShaked;

@property (nonatomic        )  CGFloat y0;
@property (nonatomic        )  CGFloat y1;
@property (nonatomic        )  CGFloat y2;

@end

@implementation PnrListVC

- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
        if (dic) {
            self.title            = dic[@"title"];
            self.weakInfoArray    = dic[@"weakInfoArray"];
            self.closeBlock       = dic[@"closeBlock"];
            self.blockExtraRecord = dic[@"blockExtraRecord"];
        }
    }
    return self;
}

+ (PnrListVC *)getPnrListVC_default {
    
    PnrBlockPVoid closeBlock = ^() { };
    
    PoporNetRecord * pnr = [PoporNetRecord share];
    
    {
        if ([PnrConfig share].vcRootTitle == nil) {
            [PnrConfig share].vcRootTitle = @"";
        }
        if (pnr.infoArray == nil) {
            pnr.infoArray = [NSMutableArray new];
        }
        if (pnr.blockExtraRecord == nil) {
            pnr.blockExtraRecord = ^(PnrEntity * _Nonnull pnrEntity) {};
        }
    }
    
    NSDictionary * dic =
    @{
        @"title":[PnrConfig share].vcRootTitle,
        @"weakInfoArray":pnr.infoArray,
        @"closeBlock":closeBlock,
        @"blockExtraRecord":pnr.blockExtraRecord,
    };
    
    PnrListVC * vc = [[PnrListVC alloc] initWithDic:dic];
    
    return vc;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (!self.navigationController.view.window) {
        if (self.closeBlock) {
            self.closeBlock();
        }
    }
}

- (void)rootVCDismissApply {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // 偷个懒, 不再管ios11 以下的设备了.
    if (@available(iOS 11, *)) {
        CGFloat top = self.infoTV.adjustedContentInset.top;
        self.y0 = -60 -top;
        self.y1 = -20 -top;
        self.y2 = -10 -top;
    } else {
        self.y0 = -60;
        self.y1 = -20;
        self.y2 = -10;
    }
}

- (void)viewDidLoad {
    self.config      = [PnrConfig share];
    self.extraEntity = [PnrExtraEntity share];
    
    self.viewImageView = ({
        UIImageView * oneIV = [UIImageView new];
        oneIV.frame = CGRectMake(0, 0, 10, 10);
        oneIV.backgroundColor = PnrColorGreen;
        oneIV.layer.cornerRadius = 5;
        oneIV.layer.masksToBounds = YES;
        
        oneIV;
    });
    
    [super viewDidLoad];
    
    if (!self.title) {
        self.title = @"PnrListVC";
    }
    
    [self addViews];
    
    {   // 返回按钮
        UIBarButtonItem *item = [[UIBarButtonItem alloc] init];
        item.title = @"";
        self.navigationItem.backBarButtonItem = item;
    }
}

- (void)addViews {
    self.infoTV = [self addTVs];
    [PnrUITool layoutSameBounds:self.infoTV superView:self.view];
    
    CGFloat bottom = [PnrUITool ncViewVisibleBottom:self.navigationController];
    if (bottom != 0) {
        UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, bottom, 0);
        self.infoTV.contentInset          = insets;
        self.infoTV.scrollIndicatorInsets = insets;
    }
    
    if ([self.navigationController.viewControllers indexOfObject:self] == 0) {
        UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"关闭" style:UIBarButtonItemStylePlain target:self action:@selector(closeAction)];
        UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"设置" style:UIBarButtonItemStylePlain target:self action:@selector(showSetVC)];
        
        self.navigationItem.leftBarButtonItems = @[item1, item2];
    }
    
    {
        self.setL = ({
            UILabel * oneL = [UILabel new];
            oneL.frame               = CGRectMake(0, -40, self.view.frame.size.width, 30);
            oneL.backgroundColor     = self.infoTV.backgroundColor; // ios8 之前
            oneL.font                = [UIFont systemFontOfSize:15];
            oneL.textColor           = UIColor.darkGrayColor;
            oneL.layer.masksToBounds = YES; // ios8 之后 lableLayer 问题
            oneL.numberOfLines       = 1;
            oneL.textAlignment       = NSTextAlignmentCenter;
            
            oneL.text = @"打开设置";
            oneL;
        });
        
        [self.infoTV addSubview:self.setL];
        
        {
            UIView * view0     = self.setL;
            UIView * superView = self.infoTV;
            view0.translatesAutoresizingMaskIntoConstraints = NO;
            
            NSLayoutConstraint *topLC = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeTop multiplier:1.0 constant:-40];
            [superView addConstraint:topLC];
            
            NSLayoutConstraint *centerXLC = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
            [superView addConstraint:centerXLC];
            
            //            NSLayoutConstraint *leftLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0];
            //            [superView addConstraint:leftLC];
            //
            //            NSLayoutConstraint *rightLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeRight multiplier:1.0 constant:0];
            //            [superView addConstraint:rightLC];
            
            //            NSLayoutConstraint *widthLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:65];
            //            [superView addConstraint:widthLC];
            
            NSLayoutConstraint *heightLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:30];
            [superView addConstraint:heightLC];
            
            //        NSLayoutConstraint * bottomLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationLessThanOrEqual toItem:superView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
            //        [superView addConstraint:bottomLC];
        }
    }
    
    [self setRightBarAction];
    
    __weak typeof(self) weakSelf = self;
    [PnrConfig share].freshBlock = ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.infoTV reloadData];
        });
    };
    
}

- (UITableView *)addTVs {
    UITableView * oneTV = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    
    oneTV.delegate   = self;
    oneTV.dataSource = self;
    
    oneTV.allowsMultipleSelectionDuringEditing = YES;
    oneTV.directionalLockEnabled = YES;
    
    oneTV.estimatedRowHeight           = 0;
    oneTV.estimatedSectionHeaderHeight = 0;
    oneTV.estimatedSectionFooterHeight = 0;
    
    [self.view addSubview:oneTV];
    
    return oneTV;
}

//------------------------------------------------------------------------------------
#pragma mark - TV_Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.weakInfoArray.count;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    PnrEntity * entity = self.weakInfoArray[self.weakInfoArray.count -  indexPath.row - 1];
    if (entity.logValue) {
        switch (self.config.jsonViewLogDetail) {
            case PnrListTypeLogDetail:{
                if (entity.logDetailH == 0) {
                    entity.logDetailH = [PnrListVCCell cellLogH:entity.logValue];
                }
                return entity.logDetailH;
                break;
            }
            case PnrListTypeLogSimply:{
                return self.config.listCellHeight;
                break;
            }
            case PnrListTypeLogNull:{
                return 0.1;
                break;
            }
            default:{
                return self.config.listCellHeight;
                break;
            }
        }
    }else{
        if (self.config.jsonViewColorBlack == PnrListTypeTextNull) {
            return 0.1;
        }else{
            return self.config.listCellHeight;
        }
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * CellID   = @"infoTV_1";
    static UIFont * cellFont15;
    if (!cellFont15) {
        cellFont15 = [UIFont systemFontOfSize:20];
    }
    PnrListVCCell * cell = [tableView dequeueReusableCellWithIdentifier:CellID];
    if (!cell) {
        cell = [[PnrListVCCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellID];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    PnrEntity * entity = self.weakInfoArray[self.weakInfoArray.count -  indexPath.row - 1];
    
    if (entity.logValue) {
        if (self.config.jsonViewLogDetail == PnrListTypeLogNull) {
            cell.hidden = YES;
            //cell.accessoryType = UITableViewCellAccessoryNone;
        }else{
            cell.hidden = NO;
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            NSMutableAttributedString * att = [NSMutableAttributedString new];
            [PnrUITool att:att string:entity.title font:self.config.listFontTitle color:self.config.listColorTitle];
            cell.requestL.attributedText = att;
            
            cell.domainL.text = entity.logValue;
        }
    }else{
        if (self.config.jsonViewColorBlack == PnrListTypeTextNull) {
            cell.hidden = YES;
            //cell.accessoryType = UITableViewCellAccessoryNone;
        }else{
            cell.hidden = NO;
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            if (entity.title) {
                NSMutableAttributedString * att = [NSMutableAttributedString new];
                
                [PnrUITool att:att string:entity.title font:self.config.listFontTitle color:self.config.listColorTitle];
                [PnrUITool att:att string:[NSString stringWithFormat:@" %@", entity.path] font:self.config.listFontRequest color:self.config.listColorRequest];
                
                cell.requestL.attributedText = att;
            }else{
                cell.requestL.text      = entity.path;
                cell.requestL.font      = self.config.listFontRequest;
                cell.requestL.textColor = self.config.listColorRequest;
            }
            cell.domainL.text  = entity.domain;
        }
    }
    cell.timeL.text = entity.time;
    
    if (indexPath.row%2 == 0) {
        cell.backgroundColor = [UIColor whiteColor];
    }else{
        cell.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PnrListVCCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    if (self.lastSelectCell != cell) {
        self.lastSelectCell.accessoryView = nil;
        self.lastSelectCell = cell;
        self.lastSelectCell.accessoryView = self.viewImageView;
    }
    
    PnrEntity * entity = self.weakInfoArray[self.weakInfoArray.count -  indexPath.row - 1];
    
    __weak typeof(self) weakSelf = self;
    [entity getJsonArrayBlock:^(NSArray *titleArray, NSArray *jsonArray, NSMutableArray *cellAttArray) {
        
        NSDictionary * vcDic = @{
            @"title":@"请求详情",
            @"jsonArray":jsonArray,
            @"titleArray":titleArray,
            @"cellAttArray":cellAttArray,
            @"blockExtraRecord":self.blockExtraRecord,
            @"weakPnrEntity":entity,
        };
        [weakSelf.navigationController pushViewController:[[PnrDetailVC alloc] initWithDic:vcDic] animated:YES];
    }];
}

#pragma mark - SV_Delegate
#pragma mark - SV 拖拽事件
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView == self.infoTV) {
        self.dragShaked = NO;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView == self.infoTV) {
        if (scrollView.contentOffset.y <= self.y0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self showSetVC];
            });
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.infoTV) {
        // NSLog(@"u : %f - 设置: %f", scrollView.contentOffset.y, self.setL.frame.origin.y);
        
        if (scrollView.contentOffset.y < self.y0) {
            self.setL.text = @"打开设置";
            self.setL.hidden = NO;
            if (!self.dragShaked) {
                self.dragShaked = YES;
                //FeedbackShakePhone;
            }
        } else if (scrollView.contentOffset.y < self.y1) {
            self.setL.text = @"设置";
            self.setL.hidden = NO;
        } else if (scrollView.contentOffset.y < self.y2) {
            self.setL.text = @"设置";
            self.setL.hidden = YES;
        }
        
    }
}

#pragma mark - VC_EventHandler
- (void)closeAction {
    UIViewController * topVC = self;
    if (topVC.navigationController) {
        if (topVC.navigationController.viewControllers.count == 1) {
            [topVC.navigationController dismissViewControllerAnimated:YES completion:nil];
        }else{
            [topVC.navigationController popViewControllerAnimated:YES];
        }
    }else{
        [topVC dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)showSetVC {
    [self.navigationController pushViewController:[PnrSetVC new] animated:YES];
}

- (void)clearAction {
    [self.weakInfoArray removeAllObjects];
    [self.infoTV reloadData];
    
    // 清空记录
    [[PnrWebServer share] clearListWeb];
}

- (void)setRightBarAction {
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"清空" style:UIBarButtonItemStylePlain target:self action:@selector(clearAction)];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"转发" style:UIBarButtonItemStylePlain target:self action:@selector(forwardeTextAction)];
    if (self.blockExtraRecord) {
        self.navigationItem.rightBarButtonItems = @[item1, item2];
    } else {
        self.navigationItem.rightBarButtonItems = @[item1];
    }
}

- (void)forwardeTextAction {
    PnrMessageVC * vc   = [PnrMessageVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

@end

//------------------------------------------------------------------------------------
@implementation PnrListVCCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self addViews];
    }
    return self;
}

- (void)addViews {
    PnrConfig * config = [PnrConfig share];
    for (int i = 0; i<3; i++) {
        UILabel * oneL = ({
            UILabel * l = [UILabel new];
            l.backgroundColor    = [UIColor clearColor];
            l.font               = [UIFont systemFontOfSize:15];
            
            [self.contentView addSubview:l];
            l;
        });
        
        switch (i) {
            case 0:{
                self.requestL  = oneL;
                oneL.textColor = config.listColorTitle;
                oneL.font      = config.listFontTitle;
                oneL.numberOfLines = 1;
                break;
            }
            case 1:{
                oneL.textAlignment = NSTextAlignmentRight;
                self.timeL     = oneL;
                oneL.textColor = config.listColorTime;
                oneL.font      = config.listFontTime;
                oneL.numberOfLines = 1;
                break;
            }
            case 2:{
                self.domainL   = oneL;
                oneL.textColor = config.listColorDomain;
                oneL.font      = config.listFontDomain;
                oneL.numberOfLines = 0;
                break;
            }
            default:
                break;
        }
    }
    float gap = PnrListCellGap;
    
    UIView * superView = self.contentView;
    {
        UIView * view0     = self.timeL;
        //UIView * superView = self;
        view0.translatesAutoresizingMaskIntoConstraints = NO;
        
        NSLayoutConstraint *topLC = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeTop multiplier:1.0 constant:gap];
        [superView addConstraint:topLC];
        
        NSLayoutConstraint *rightLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-0];
        [superView addConstraint:rightLC];
        
        NSLayoutConstraint *widthLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:65];
        [superView addConstraint:widthLC];
        
        //        NSLayoutConstraint *heightLC  = [NSLayoutConstraint constraintWithItem:self.timeL attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:18];
        //        [self.contentView addConstraint:heightLC];
        
        //        NSLayoutConstraint * bottomLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationLessThanOrEqual toItem:superView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
        //        [superView addConstraint:bottomLC];
    }
    
    {
        UIView * view0     = self.requestL;
        //UIView * superView = self;
        view0.translatesAutoresizingMaskIntoConstraints = NO;
        
        NSLayoutConstraint *topLC = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeTop multiplier:1.0 constant:gap];
        [superView addConstraint:topLC];
        
        NSLayoutConstraint *leftLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:PnrListCellTextLeftGap];
        [superView addConstraint:leftLC];
        
        NSLayoutConstraint *rightLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.timeL attribute:NSLayoutAttributeLeft multiplier:1 constant:-5];
        [superView addConstraint:rightLC];
        
        //        NSLayoutConstraint *heightLC  = [NSLayoutConstraint constraintWithItem:self.timeL attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:18];
        //        [self.contentView addConstraint:heightLC];
        
        //        NSLayoutConstraint * bottomLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationLessThanOrEqual toItem:superView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
        //        [superView addConstraint:bottomLC];
    }
    
    {
        UIView * view0     = self.domainL;
        //UIView * superView = self;
        view0.translatesAutoresizingMaskIntoConstraints = NO;
        
        NSLayoutConstraint *topLC = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.requestL attribute:NSLayoutAttributeBottom multiplier:1.0 constant:gap];
        [superView addConstraint:topLC];
        
        NSLayoutConstraint *leftLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:PnrListCellTextLeftGap];
        [superView addConstraint:leftLC];
        
        NSLayoutConstraint *rightLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.timeL attribute:NSLayoutAttributeRight multiplier:1.0 constant:0];
        [superView addConstraint:rightLC];
        
        NSLayoutConstraint *bottomLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeBottom multiplier:1 constant:-gap];
        [superView addConstraint:bottomLC];
        
        //        NSLayoutConstraint *heightLC  = [NSLayoutConstraint constraintWithItem:self.timeL attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:18];
        //        [self.contentView addConstraint:heightLC];
        
        //        NSLayoutConstraint * bottomLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationLessThanOrEqual toItem:superView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
        //        [superView addConstraint:bottomLC];
    }
}

+ (int)cellLogH:(NSString *)log {
    static int textWidth;
    
    static PnrConfig * config;
    if (textWidth == 0) {
        textWidth = [[UIScreen mainScreen] bounds].size.width - PnrListCellTextLeftGap - PnrListCellTextRightGap;
        config = [PnrConfig share];
    }
    
    if (log) {
        return config.listCellHeight - config.listFontTitle.pointSize
        + [PnrUITool sizeString:log font:config.listFontDomain width:textWidth].height;
        
        //+ [log sizeInFont:config.listFontDomain width:textWidth].height;
    }else{
        return 44;
    }
}

@end
