//
//  PnrMessageVC.m
//  PoporNetRecord
//
//  Created by popor on 2019/12/6.
//  Copyright © 2019 popor. All rights reserved.

#import "PnrMessageVC.h"
#import "PnrUITool.h"
#import "PoporNetRecord.h"

@interface PnrMessageVC ()

@end

@implementation PnrMessageVC

- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
        if (dic) {
            self.title = dic[@"title"];
        }
    }
    return self;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    if (self.textTV.frame.size.width +32 != self.view.frame.size.width) {
        self.textTV.frame = CGRectMake(16, self.textTV.frame.origin.y , self.view.frame.size.width - 32, 200);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.title) {
        self.title = @"转发文本";
    }
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self addViews];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.textTV becomeFirstResponder];
    });
}

- (void)addViews {
    
    self.textTV = ({
        UITextView * tv = [UITextView new];
        tv.layer.borderColor = [UIColor grayColor].CGColor;
        tv.layer.borderWidth = 1;
        
        tv.layer.cornerRadius = 5;
        tv.clipsToBounds = YES;
        tv.font = [UIFont systemFontOfSize:15];
        
        [self.view addSubview:tv];
        tv;
    });
    CGFloat y = 20;
    if (self.navigationController.navigationBar.translucent) {
        y = y + self.navigationController.navigationBar.frame.size.height +[PnrUITool statusBarHeight];
    }
    
    self.textTV.frame = CGRectMake(16, y , self.view.frame.size.width - 32, 200);
    
    
    self.statusL = ({
        UILabel * oneL = [UILabel new];
        oneL.backgroundColor     = [UIColor blackColor]; // ios8 之前
        oneL.font                = [UIFont systemFontOfSize:14];
        oneL.textColor           = UIColor.whiteColor;
        oneL.layer.masksToBounds = YES; // ios8 之后 lableLayer 问题
        oneL.numberOfLines       = 1;
        oneL.textAlignment       = NSTextAlignmentCenter;
        oneL.text                = @"已发送";
        oneL.alpha               = 0;
        
        oneL.layer.cornerRadius  = 6;
        oneL.layer.masksToBounds = YES;
        
        [self.view addSubview:oneL];
        oneL;
    });
    self.statusL.frame = CGRectMake((self.view.frame.size.width - 90)/2, CGRectGetMaxY(self.textTV.frame) + 20, 80, 30);
    
    {
        UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"发送" style:UIBarButtonItemStylePlain target:self action:@selector(sendAction)];
        self.navigationItem.rightBarButtonItems = @[item1];
    }
}

// -----------------------------------------------------------------------------

- (void)sendAction {
    
    [PoporNetRecord addLog:self.textTV.text title:@"转发文本"];
    
    
    // alert
    [UIView animateWithDuration:0.2 animations:^{
        self.statusL.alpha = 1;
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.2 animations:^{
            self.statusL.alpha = 0;
        }];
    });
    
}

@end
