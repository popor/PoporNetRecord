//
//  PoporNetRecord.m
//  PoporNetRecord
//
//  Created by popor on 2018/5/16.
//  Copyright © 2018年 popor. All rights reserved.
//

#import "PoporNetRecord.h"
#import "PnrEntity.h"
#import "PnrWebServer.h"

#import "PnrListVC.h"
#import "PnrUITool.h"

#define LL_SCREEN_WIDTH  [[UIScreen mainScreen] bounds].size.width
#define LL_SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height

@interface PoporNetRecord () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSMutableString * listWebH5;

@end

@implementation PoporNetRecord

+ (instancetype)share {
    static dispatch_once_t once;
    static PoporNetRecord * instance;
    dispatch_once(&once, ^{
        instance = [self new];
        instance.infoArray = [NSMutableArray new];
        instance.listWebH5 = [NSMutableString new];
        
        instance.config    = [PnrConfig share];
        
        // 相关联的关联数组
        [PnrWebServer share].infoArray = instance.infoArray;
        
        {// 设置默认的请求方式
            PnrEntityMethod * m0 = [[PnrEntityMethod alloc] initIndex:0 value:@"Get"];
            PnrEntityMethod * m1 = [[PnrEntityMethod alloc] initIndex:1 value:@"POST(Json)"];
            PnrEntityMethod * m2 = [[PnrEntityMethod alloc] initIndex:2 value:@"Post(FormData)"];
            PnrEntityMethod * m3 = [[PnrEntityMethod alloc] initIndex:3 value:@"Post(XForm) "];
            PnrEntityMethod * m4 = [[PnrEntityMethod alloc] initIndex:4 value:@"Put"];
            
            [PoporNetRecord setMethodArray:@[m0, m1, m2, m3, m4]];
        }
        
    });
    return instance;
}

+ (void)addUrl:(NSString *)urlString method:(NSInteger)method head:(id _Nullable)headValue parameter:(id _Nullable)parameterValue response:(id _Nullable)responseValue
{
    [self addUrl:urlString title:@"--" method:method head:headValue parameter:parameterValue response:responseValue];
}

+ (void)addUrl:(NSString *)urlString title:(NSString *)title method:(NSInteger)method head:(id _Nullable)headValue parameter:(id _Nullable)parameterValue response:(id _Nullable)responseValue
{
    PoporNetRecord * pnr = [PoporNetRecord share];
    if (pnr.config.isRecord) {
        PnrEntity * entity = [PnrEntity new];
        entity.title          = title;
        entity.url            = urlString;
        entity.method         = method;
        
        for (PnrEntityMethod * unit in [PnrConfig share].methodArray) {
            if (unit.index == method) {
                entity.methodName = unit.value;
                break;
            }
        }
        
        entity.headValue      = headValue;
        entity.parameterValue = parameterValue;
        entity.responseValue  = responseValue;
        entity.time           = [PnrUITool currentTime];
        entity.time_double    = [PnrUITool getCurrentUnixDate];
        
        if (urlString.length>0) {
            NSURL * url = [NSURL URLWithString:urlString];
            if (url.baseURL) {
                entity.domain = [NSString stringWithFormat:@"%@://%@", url.scheme, url.host];
                
                if (entity.domain.length+1 < urlString.length) {
                    entity.path = [urlString substringFromIndex:entity.domain.length+1];
                    NSString * query = url.query;
                    if (query.length > 0) {
                        entity.path = [entity.path substringToIndex:entity.path.length-1-query.length];
                    }
                }
            }else{
                entity.domain = urlString;
                entity.path   = @"";
            }
        }
        if (pnr.infoArray.count == 0) {
            // 当执行了数组清空之后, h5代码清零一次.
            [pnr.listWebH5 setString:@""];
        }
        [pnr.infoArray addObject:entity];
        
        if (pnr.config.isShowListWeb) {
            [entity createListWebH5:pnr.infoArray.count - 1];
            [pnr.listWebH5 insertString:entity.listWebH5 atIndex:0];
            [[PnrWebServer share] startListServer:pnr.listWebH5];
            [self startWebServer];
        }else{
            [self stopWebServer];
        }
        
        // 假如在打开界面的时候收到请求,那么刷新数据
        if (pnr.config.freshBlock) {
            pnr.config.freshBlock();
        }
        if (pnr.blockExtraRecord) {
            entity.deviceName = [[UIDevice currentDevice] name];
            pnr.blockExtraRecord(entity);
        }
        
        
        if (pnr.requestRatePerSecond > 0 && pnr.blockException) {
            PnrEntity * peLast = pnr.infoArray.lastObject;
            
            BOOL hasException = NO;
            CGFloat currentPerSecond = 0;
            for (NSInteger i = 3; i<6; i++) {
                NSInteger index = (NSInteger) (i*pnr.requestRatePerSecond);
                if (pnr.infoArray.count > index) {
                    PnrEntity * peTemp = pnr.infoArray[pnr.infoArray.count -1 -index];
                    CGFloat     gap    = MAX(1, peLast.time_double - peTemp.time_double);// 小于1秒内的, 可能数量会爆炸.
                    if (gap > 0) {
                        currentPerSecond = index/gap;
                        if (currentPerSecond > pnr.requestRatePerSecond) {
                            hasException = YES;
                            break;
                        }
                    }
                }
            }
            
            if (hasException) {
                pnr.blockException(currentPerSecond, pnr.requestRatePerSecond);
            }
        }
    }
}

+ (void)setMethodArray:(NSArray<PnrEntityMethod *> *)array {
    [PnrConfig share].methodArray = array;
}

+ (void)setPnrBlockResubmit:(PnrBlockResubmit _Nullable)block extraDic:(NSDictionary * _Nullable)dic {
    [PnrWebServer share].resubmitBlock = block;
    [PnrWebServer share].resubmitExtraDic = dic;
}


+ (void)startWebServer {
    PoporNetRecord * pnr = [PoporNetRecord share];
    [[PnrWebServer share] startListServer:pnr.listWebH5];
}

+ (void)stopWebServer {
    [[PnrWebServer share] stopServer];
}

// Log 部分

+ (void)addLog:(NSString *)log {
    [self addLog:log title:@"日志"];
}

+ (void)addLog:(NSString *)log title:(NSString *)title
{
    PoporNetRecord * pnr = [PoporNetRecord share];
    if (pnr.config.isRecord) {
        PnrEntity * entity = [PnrEntity new];
        if ([log isKindOfClass:[NSString class]]) {
            entity.logValue = log;
        } else if ([log isKindOfClass:[NSDictionary class]]) {
            NSData * data   = [NSJSONSerialization dataWithJSONObject:(NSDictionary *)log options:kNilOptions error:nil];
            entity.logValue = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            entity.logDic   = (NSDictionary *)log;
        } else {
            entity.logValue = @"{}";
        }
        
        entity.title    = title;
        entity.url      = @"无链接";
        entity.time     = [PnrUITool currentTime];
        
        if (pnr.infoArray.count == 0) {
            // 当执行了数组清空之后, h5代码清零一次.
            [pnr.listWebH5 setString:@""];
        }
        [pnr.infoArray addObject:entity];
        
        if (pnr.config.isShowListWeb) {
            [entity createListWebH5:pnr.infoArray.count - 1];
            [pnr.listWebH5 insertString:entity.listWebH5 atIndex:0];
            [[PnrWebServer share] startListServer:pnr.listWebH5];
        }else{
            [[PnrWebServer share] stopServer];
        }
        
        // 假如在打开界面的时候收到请求,那么刷新数据
        if (pnr.config.freshBlock) {
            pnr.config.freshBlock();
        }
        if (pnr.blockExtraRecord) {
            entity.deviceName = [[UIDevice currentDevice] name];
            pnr.blockExtraRecord(entity);
        }
    }
}


// 监测网络请求频率, 假如发现连续3秒, 每秒超过perSecond, 则反馈. 最长监测4秒内.
+ (void)watchRequestRate:(CGFloat)perSecond exception:(PnrBlockPFloatFloat)blockException {
    PoporNetRecord * pnr = [PoporNetRecord share];
    pnr.requestRatePerSecond = perSecond;
    pnr.blockException       = blockException;
    
}

@end
