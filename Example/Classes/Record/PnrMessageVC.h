//
//  PnrMessageVC.h
//  PoporNetRecord
//
//  Created by popor on 2019/12/6.
//  Copyright © 2019 popor. All rights reserved.

#import <UIKit/UIKit.h>
#import "PnrPrefix.h"

@interface PnrMessageVC : UIViewController 

// MARK: 自己的
@property (nonatomic, strong) UITextView * textTV;
@property (nonatomic, strong) UILabel    * statusL;



- (instancetype)initWithDic:(NSDictionary *)dic;


@end
