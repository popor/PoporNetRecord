//
//  PnrExtraVC.m
//  PoporNetRecord
//
//  Created by popor on 2019/11/16.
//  Copyright © 2019 popor. All rights reserved.

#import "PnrSetVC.h"
#import "PnrUITool.h"

#import "PnrConfig.h"
#import "PnrPortEntity.h"
#import "PnrExtraEntity.h"
#import "PoporNetRecord.h"
#import "PnrCellEntity.h"
#import "PnrSV.h"
#import "PnrWebServer.h"

@interface PnrSetVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) PnrSV          * bgSV;
@property (nonatomic, strong) UITableView    * funTV;
@property (nonatomic, strong) UITableView    * viewTV;

@property (nonatomic, weak  ) PnrExtraEntity * extraEntity;
@property (nonatomic, weak  ) PnrConfig      * pnrConfig;
@property (nonatomic, strong) UISegmentedControl * sc;

@property (nonatomic, strong) NSArray        * viewTVArray;

@end

@implementation PnrSetVC

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    if (self.bgSV.frame.size.width != self.view.frame.size.width) {
        CGFloat top = self.bgSV.frame.origin.y;
        self.bgSV.frame = CGRectMake(0, top, self.view.frame.size.width, self.view.frame.size.height -top);
        self.bgSV.contentSize = CGSizeMake(self.bgSV.frame.size.width *2, self.bgSV.frame.size.height);
        
        self.funTV.frame  = self.bgSV.bounds;
        self.viewTV.frame = CGRectMake(self.funTV.frame.size.width, self.funTV.frame.origin.y, self.funTV.frame.size.width, self.funTV.frame.size.height);
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.title) {
        self.title = @"设置";
    }
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.extraEntity = [PnrExtraEntity share];
    self.pnrConfig   = [PnrConfig share];
    
    {
        self.viewTVArray =
        @[
            @[
                [PnrCellEntity type :PnrListTypeTextColor title :@"彩色 :富文本会占用大量内存"],
                [PnrCellEntity type :PnrListTypeTextBlack title :@"黑色 :低内存"],
                [PnrCellEntity type :PnrListTypeTextNull  title :@"不在列表中显示"],
            ],
            @[
                [PnrCellEntity type :PnrListTypeLogDetail title :@"详细: 列表页显示详细信息"],
                [PnrCellEntity type :PnrListTypeLogSimply title :@"简化: 列表页值显示一行信息"],
                [PnrCellEntity type :PnrListTypeLogNull   title :@"不在列表中显示"],
            ]
        ];
    }

    CGFloat bottom = [PnrUITool ncViewVisibleBottom:self.navigationController];
    CGFloat top    = [PnrUITool ncViewVisibleTop:self.navigationController];
    
    {
        self.bgSV = ({
            PnrSV * sv = [PnrSV new];
            sv.frame         = CGRectMake(0, top, self.view.frame.size.width, self.view.frame.size.height -top);
            sv.contentSize   = CGSizeMake(sv.frame.size.width *2, sv.frame.size.height);
            sv.pagingEnabled = YES;
            sv.directionalLockEnabled = YES;
            sv.delegate = self;
            sv.showsVerticalScrollIndicator = NO;
            sv.showsHorizontalScrollIndicator = NO;
            
            
            [sv.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
            
            [self.view addSubview:sv];
            sv;
        });
        self.funTV  = [self addTVs];
        self.viewTV = [self addTVs];
        
        if (bottom != 0) {
            UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, bottom, 0);
            self.funTV.contentInset           = insets;
            self.viewTV.contentInset          = insets;
            self.funTV.scrollIndicatorInsets  = insets;
            self.viewTV.scrollIndicatorInsets = insets;
        }
        
        self.funTV.frame  = self.bgSV.bounds;
        self.viewTV.frame = CGRectMake(self.funTV.frame.size.width, self.funTV.frame.origin.y, self.funTV.frame.size.width, self.funTV.frame.size.height);
        
        [self.bgSV addSubview:self.funTV];
        [self.bgSV addSubview:self.viewTV];
    }
    [self addTitleView];
    
    
    [self.bgSV.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
}

- (void)addTitleView {
    self.sc = [[UISegmentedControl alloc] initWithItems:@[@"功能", @"显示"]];
    self.sc.backgroundColor = self.pnrConfig.segmentedControl_backgroundColor ? : UIColor.clearColor;
    self.sc.tintColor       = self.pnrConfig.segmentedControl_tintColor ? : UIColor.whiteColor;
    
    // if (@available(iOS 13, *)) {
    //     self.sc.selectedSegmentTintColor = self.pnrConfig.segmentedControl_tintColor ? : UIColor.whiteColor;
    // } else {
    //     self.sc.tintColor = self.pnrConfig.segmentedControl_tintColor ? : UIColor.whiteColor;
    // }
    
    self.sc.selectedSegmentIndex     = 0;
    
    [self.sc setTitleTextAttributes:@{NSForegroundColorAttributeName: self.pnrConfig.segmentedControl_titleColor_normal ? : UIColor.blackColor} forState:UIControlStateNormal];

    [self.sc setTitleTextAttributes:@{NSForegroundColorAttributeName: self.pnrConfig.segmentedControl_titleColor_selected ? : UIColor.blackColor} forState:UIControlStateSelected];
    
    //self.sc
    
    [self.sc addTarget:self action:@selector(changeIndex) forControlEvents:UIControlEventValueChanged];
    self.navigationItem.titleView = self.sc;
}

// -----------------------------------------------------------------------------
- (UITableView *)addTVs {
    UITableView * oneTV = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    
    oneTV.backgroundColor = [UIColor clearColor];
    //oneTV.separatorColor  = [UIColor clearColor];
    oneTV.delegate   = self;
    oneTV.dataSource = self;
    
    oneTV.allowsMultipleSelectionDuringEditing = YES;
    oneTV.directionalLockEnabled = YES;
    
    oneTV.estimatedRowHeight           = 0;
    oneTV.estimatedSectionHeaderHeight = 0;
    oneTV.estimatedSectionFooterHeight = 0;
    
    return oneTV;
}

// -- Delegate ---------------------------------------------------------------------------
#pragma mark - TV_Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.funTV) {
        return 3;
    } else {
        return self.viewTVArray.count;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.funTV) {
        switch (section) {
            case 0: {
                return 3;
            }
            case 1: {
                return 3;
            }
            case 2: {
                return self.extraEntity.urlPortArray.count +2;
            }
            default:
                return 0;
        }
    } else {
        return 3;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.funTV) {
        switch (indexPath.section) {
            case 0:{
                return 40;
            }
            case 1: {
                switch (indexPath.row) {
                    case 0:
                        return 50;
                        break;
                    case 1:
                        return 40;
                        break;
                    case 2:
                        return 40;
                        break;
                    default:
                        break;
                }
                break;
            }
            case 2:{
                if (indexPath.row == 0) {
                    return 40;
                } else if(indexPath.row +1 == [self tableView:tableView numberOfRowsInSection:indexPath.section]) {
                    return 40;
                } else {
                    return 50;
                }
                break;
            }
            default:
                break;
        }
        return 40;
    } else {
        return 40;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == self.funTV) {
        return 50;
    } else {
        return 50;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == self.funTV) {
        switch (section) {
            case 0: {
                return @"📡 数据监测";// 🔎
            }
            case 1: {
                return @"🌐 Web服务";// 🌐
            }
            case 2: {
                return @"⤴️ 数据转发";// ⤴️
            }
            default:
                return nil;
        }
    } else {
        switch (section) {
            case 0: {
                return @"🖥 网络请求: 详情颜色和开关";
            }
            case 1: {
                return @"🖥 Log日志: 显示风格和开关";
            }
            default:
                return nil;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section +1 == [self numberOfSectionsInTableView:tableView] ) {
        return 20;
        //return 20 + [PnrUITool safeAreaInsets].bottom;
    } else {
        return 0.1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.funTV) {
        switch (indexPath.section) {
            case 0:{
                static NSString * CellID = @"CellID0";
                UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellID];
                if (!cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellID];
                    cell.accessoryType        = UITableViewCellAccessoryCheckmark;
                    cell.selectionStyle       = UITableViewCellSelectionStyleNone;
                    cell.textLabel.font       = [UIFont systemFontOfSize:14];
                    cell.detailTextLabel.font = [UIFont systemFontOfSize:16];
                }
                
                switch (indexPath.row) {
                    case 0: {
                        cell.textLabel.text = @"自动: 真机Release版本不开启";
                        if (self.pnrConfig.recordType == PoporNetRecordAuto) {
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                            cell.textLabel.textColor  = cell.tintColor;
                        } else {
                            cell.accessoryType = UITableViewCellAccessoryNone;
                            cell.textLabel.textColor  = [UIColor grayColor];
                        }
                        break;
                    }
                    case 1: {
                        cell.textLabel.text = @"开启";
                        if (self.pnrConfig.recordType == PoporNetRecordEnable) {
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                            cell.textLabel.textColor  = cell.tintColor;
                        } else {
                            cell.accessoryType = UITableViewCellAccessoryNone;
                            cell.textLabel.textColor  = [UIColor grayColor];
                        }
                        break;
                    }
                    case 2: {
                        cell.textLabel.text = @"关闭";
                        if (self.pnrConfig.recordType == PoporNetRecordDisable) {
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                            cell.textLabel.textColor  = cell.tintColor;
                        } else {
                            cell.accessoryType = UITableViewCellAccessoryNone;
                            cell.textLabel.textColor  = [UIColor grayColor];
                        }
                        break;
                    }
                    default:
                        break;
                }
                
                return cell;
            }
            case 1:{
                switch (indexPath.row) {
                    case 0: {
                        static NSString * CellID = @"CellID1_0";
                        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellID];
                        if (!cell) {
                            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellID];
                            cell.selectionStyle       = UITableViewCellSelectionStyleNone;
                            cell.textLabel.font       = [UIFont systemFontOfSize:14];
                            cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
                            
                            cell.detailTextLabel.textColor  = [UIColor grayColor];
                            cell.detailTextLabel.text = @"开启后，Xcode会打印很多Log日志。";
                        }
                        
                        if (self.pnrConfig.webType == PoporNetRecordEnable) {
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                            cell.textLabel.textColor  = cell.tintColor;
                            PnrWebServer * server = [PnrWebServer share];
                            NSString * url = [server serverUrl];
                            if (url.length > 0) {
                                cell.textLabel.text = [NSString stringWithFormat:@"已开启: %@", url];
                            } else {
                                cell.textLabel.text = @"已开启";
                            }
                            
                        } else {
                            cell.accessoryType = UITableViewCellAccessoryNone;
                            cell.textLabel.textColor  = [UIColor grayColor];
                            cell.textLabel.text       = @"开启";
                        }
                        cell.detailTextLabel.textColor = cell.textLabel.textColor;
                        return cell;
                        break;
                    }
                    case 1: {
                        static NSString * CellID = @"CellID1_1";
                        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellID];
                        if (!cell) {
                            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellID];
                            cell.selectionStyle       = UITableViewCellSelectionStyleNone;
                            cell.textLabel.font       = [UIFont systemFontOfSize:14];
                            cell.detailTextLabel.font = [UIFont systemFontOfSize:16];
                            
                            cell.textLabel.textColor  = [UIColor grayColor];
                            cell.detailTextLabel.text = nil;
                        }
                        
                        if (self.pnrConfig.webType == PoporNetRecordDisable) {
                            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                            cell.textLabel.textColor  = cell.tintColor;
                            cell.textLabel.text       = @"已关闭";
                        } else {
                            cell.accessoryType = UITableViewCellAccessoryNone;
                            cell.textLabel.textColor  = [UIColor grayColor];
                            cell.textLabel.text       = @"关闭";
                        }
                        return cell;
                        break;
                    }
                    case 2: {
                        static NSString * CellID = @"CellID1_2";
                        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellID];
                        if (!cell) {
                            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellID];
                            cell.accessoryType        = UITableViewCellAccessoryNone;
                            
                            cell.textLabel.font       = [UIFont systemFontOfSize:14];
                            cell.detailTextLabel.font = [UIFont systemFontOfSize:16];
                            
                            cell.textLabel.textColor  = [UIColor darkGrayColor];
                            cell.textLabel.text = @"端口号";
                        }
                        
                        cell.detailTextLabel.text = [NSString stringWithFormat:@"%i", [PnrPortEntity getPort_get]];
                        
                        return cell;
                        break;
                    }
                    default:
                        break;
                }
                
                return nil;
            }
            case 2:{
                static NSString * CellID = @"CellID2";
                UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellID];
                if (!cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellID];
                    cell.accessoryType        = UITableViewCellAccessoryCheckmark;
                    
                    cell.textLabel.font       = [UIFont systemFontOfSize:14];
                    cell.detailTextLabel.font = [UIFont systemFontOfSize:16];
                    
                    cell.textLabel.textColor  = [UIColor grayColor];
                }
                
                cell.tag = -1;
                if (indexPath.row == 0) {
                    PnrExtraEntity * e = [PnrExtraEntity share];
                    NSString * title   = e.forward ? @"已打开":@"已关闭";
                    UIColor  * color   = e.forward ? PnrColorGreen:PnrColorRed;
                    static UIColor * grayColor;
                    static UIFont  * font;
                    if (!grayColor) {
                        grayColor = [UIColor grayColor];
                        font      = [UIFont systemFontOfSize:15];
                    }
                    {
                        NSMutableAttributedString * att = [NSMutableAttributedString new];
                        [PnrUITool att:att string:title          font:font color:color];
                        [PnrUITool att:att string:@" 转发请求开关" font:font color:grayColor];
                        
                        cell.textLabel.attributedText = att;
                    }
                    cell.detailTextLabel.text = nil;
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    
                } else if(indexPath.row +1 == [self tableView:tableView numberOfRowsInSection:indexPath.section]) {
                    cell.textLabel.text = @"新增转发链接";
                    cell.detailTextLabel.text = nil;
                    
                    cell.accessoryType = UITableViewCellAccessoryNone;
                } else {
                    PnrExtraUrlPortEntity * ue = self.extraEntity.urlPortArray[indexPath.row -1];
                    cell.textLabel.text        = ue.title;
                    cell.detailTextLabel.text  = [NSString stringWithFormat:@"%@:%@/%@", ue.url, ue.port, ue.api];
                    
                    if (self.extraEntity.selectNum == indexPath.row -1) {
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    } else {
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                    cell.tag = PnrSV_ignore_tag;
                }
                return cell;
            }
            default:
                return nil;
        }
    } else {
        static NSString * CellID = @"alertTV";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellID];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellID];
            cell.backgroundColor     = [UIColor clearColor];
            cell.textLabel.font      = [UIFont systemFontOfSize:15];
            cell.textLabel.textColor = [UIColor darkGrayColor];
            //cell.tintColor           = [UIColor darkGrayColor];
            //cell.contentView.backgroundColor = UIColor.redColor;
        }
        NSArray       * array      = self.viewTVArray[indexPath.section];
        PnrCellEntity * cellEntity = array[indexPath.row];
        
        cell.textLabel.text = cellEntity.title;
        
        switch (cellEntity.type) {
            case PnrListTypeTextColor:
            case PnrListTypeTextBlack:
            case PnrListTypeTextNull: {
                if (self.pnrConfig.jsonViewColorBlack == cellEntity.type) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    cell.textLabel.textColor  = cell.tintColor;
                }else{
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    cell.textLabel.textColor  = [UIColor darkGrayColor];
                }
                break;
            }
                
            case PnrListTypeLogDetail:
            case PnrListTypeLogSimply:
            case PnrListTypeLogNull: {
                if (self.pnrConfig.jsonViewLogDetail == cellEntity.type) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    cell.textLabel.textColor  = cell.tintColor;
                }else{
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    cell.textLabel.textColor  = [UIColor darkGrayColor];
                }
                break;
            }
                
            default:
                break;
        }
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.funTV) {
        switch (indexPath.section) {
            case 0: {
                switch (indexPath.row) {
                    case 0: {
                        self.pnrConfig.recordType = PoporNetRecordAuto;
                        break;
                    }
                    case 1: {
                        self.pnrConfig.recordType = PoporNetRecordEnable;
                        break;
                    }
                    case 2: {
                        self.pnrConfig.recordType = PoporNetRecordDisable;
                        break;
                    }
                    default:
                        break;
                }
                [tableView reloadSections:[[NSIndexSet alloc] initWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
                break;
            }
            case 1: {
                switch (indexPath.row) {
                    case 0: {
                        if (self.pnrConfig.webType != PoporNetRecordEnable) {
                            self.pnrConfig.webType = PoporNetRecordEnable;
                        
                            [tableView reloadSections:[[NSIndexSet alloc] initWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
                            
                            [PoporNetRecord startWebServer];
                        }
                        
                        [tableView reloadData];
                        break;
                    }
                    case 1: {
                        if (self.pnrConfig.webType != PoporNetRecordDisable) {
                            self.pnrConfig.webType = PoporNetRecordDisable;
                            
                            [tableView reloadSections:[[NSIndexSet alloc] initWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
                            
                            [PoporNetRecord stopWebServer];
                        }
                        
                        [tableView reloadData];
                        break;
                    }
                    case 2: {
                        [self editPortAction];
                        break;
                    }
                    default:
                        break;
                }
                break;
            }
            case 2: {
                if (indexPath.row == 0) {
                    [self forwardAction];
                } else if(indexPath.row +1 == [self tableView:tableView numberOfRowsInSection:indexPath.section]) {
                    [self addUrlPortAction:nil indexPath:nil];
                } else {
                    if (indexPath.row -1 != self.extraEntity.selectNum) {
                        NSIndexPath * ipOld = [NSIndexPath indexPathForRow:self.extraEntity.selectNum +1 inSection:indexPath.section];
                        
                        UITableViewCell * cellOld = [tableView cellForRowAtIndexPath :ipOld];
                        UITableViewCell * cellNew = [tableView cellForRowAtIndexPath :indexPath];
                        cellOld.accessoryType     = UITableViewCellAccessoryNone;
                        cellNew.accessoryType     = UITableViewCellAccessoryCheckmark;
                        
                        [[PnrExtraEntity share] saveSelectNum:indexPath.row -1];
                        
                        [tableView reloadRowsAtIndexPaths:@[ipOld, indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }
                }
                break;
            }
            default:
                break;
        }
    } else {
        NSArray       * array      = self.viewTVArray[indexPath.section];
        PnrCellEntity * cellEntity = array[indexPath.row];
        switch (cellEntity.type) {
            case PnrListTypeTextColor:
            case PnrListTypeTextBlack:
            case PnrListTypeTextNull:{
                [self.pnrConfig updateTextColorBlack:cellEntity.type];
                [tableView reloadData];
                break;
            }
                
            case PnrListTypeLogDetail:
            case PnrListTypeLogSimply:
            case PnrListTypeLogNull:{
                [self.pnrConfig updateLogDetail:cellEntity.type];
                [tableView reloadData];
                break;
            }
            default:
                break;
        }
    }
    
}

#pragma mark - tv 删除
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.funTV) {
        if (indexPath.section == 2) {
            if (indexPath.row == 0) {
                return NO;
            } else if(indexPath.row +1 == [self tableView:tableView numberOfRowsInSection:indexPath.section]) {
                return NO;
            } else {
                return YES;
            }
            return YES;
        } else {
            return NO;
        }
    } else {
        return NO;
    }
   
}
// 只要实现了这个方法，左滑出现按钮的功能就有了
// (一旦左滑出现了N个按钮，tableView就进入了编辑模式, tableView.editing = YES)
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.funTV) {
        if (tableView == self.funTV) {
            PnrExtraUrlPortEntity * ue = self.extraEntity.urlPortArray[indexPath.row -1];
            __weak typeof(self) weakSelf = self;
            
            UITableViewRowAction *action00 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"修改" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                [weakSelf addUrlPortAction:ue indexPath:indexPath];
            }];
            
            action00.backgroundColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
            
            UITableViewRowAction *action1 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                
                [weakSelf deleteEntity:ue];
            }];
            if (indexPath.row == 1) {
                return @[action00];
            } else {
                return @[action1, action00];
            }
            
        }else{
            return nil;
        }
    } else {
        return nil;
    }
}

- (void)deleteEntity:(PnrExtraUrlPortEntity *)ue {
    [self.extraEntity.urlPortArray removeObject:ue];
    [self.extraEntity saveArray];
    if (self.extraEntity.selectNum >= self.extraEntity.urlPortArray.count) {
        self.extraEntity.selectNum = 0;
    }
    [self.funTV reloadData];
}

#pragma mark - sv
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.bgSV) {
        NSInteger index = scrollView.contentOffset.x/scrollView.frame.size.width;
        self.sc.selectedSegmentIndex = index;
    }
}

- (void)changeIndex {
    self.bgSV.contentOffset = CGPointMake(self.bgSV.frame.size.width *self.sc.selectedSegmentIndex, self.bgSV.contentOffset.y);
}

#pragma mark - VC_EventHandler
- (void)addUrlPortAction:(PnrExtraUrlPortEntity *)ue indexPath:(NSIndexPath *)indexPath
{
    UIAlertController * oneAC = [UIAlertController alertControllerWithTitle:@"新增配置" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [oneAC addTextFieldWithConfigurationHandler:^(UITextField *textField){
        
        textField.placeholder = @"标题";
        textField.text = ue ? ue.title : @"新增标题";
    }];
    
    [oneAC addTextFieldWithConfigurationHandler:^(UITextField *textField){
        
        textField.placeholder = @"域名";
        textField.text = ue ? ue.url : @"http://192.168.";
    }];
    [oneAC addTextFieldWithConfigurationHandler:^(UITextField *textField){
        
        textField.placeholder = @"端口号";
        textField.text = ue ? ue.port : @"9000";
    }];
    [oneAC addTextFieldWithConfigurationHandler:^(UITextField *textField){
        
        textField.placeholder = @"接口";
        textField.text = ue ? ue.api : @"api";
    }];
    
    UIAlertAction * cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction * changeAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField * titleTF = oneAC.textFields[0];
        UITextField * urlTF   = oneAC.textFields[1];
        UITextField * portTF  = oneAC.textFields[2];
        UITextField * apiTF   = oneAC.textFields[3];
        
        if (ue) {
            ue.title = titleTF.text;
            ue.url   = urlTF.text;
            ue.port  = portTF.text;
            ue.api   = apiTF.text;
            
            PnrExtraEntity * pee = [PnrExtraEntity share];
            if (indexPath.row-1 == pee.selectNum) {
                [pee updateSelectUrlPort];
            }
        } else {
            PnrExtraUrlPortEntity * ue_add = [PnrExtraUrlPortEntity new];
            ue_add.title = titleTF.text;
            ue_add.url   = urlTF.text;
            ue_add.port  = portTF.text;
            ue_add.api   = apiTF.text;
            
            [[PnrExtraEntity share].urlPortArray addObject:ue_add];
        }
        
        [[PnrExtraEntity share] saveArray];
        
        [self.funTV reloadData];
    }];
    
    [oneAC addAction:cancleAction];
    [oneAC addAction:changeAction];
    
    [self presentViewController:oneAC animated:YES completion:nil];
    
}

- (void)forwardAction {
    PnrExtraEntity * e = [PnrExtraEntity share];
    e.forward = !e.forward;
    [e saveForward];
    
    NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:2];
    [self.funTV reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)editPortAction {
    NSString * message = @"日志端口修改后，下次启动生效";// @"Get端口和Post端口\n修改后，下次启动生效"
    UIAlertController * oneAC = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [oneAC addTextFieldWithConfigurationHandler:^(UITextField *textField){
        
        textField.placeholder = [NSString stringWithFormat:@"%i", PnrPortGet];
        textField.text        = [NSString stringWithFormat:@"%i", [PnrPortEntity getPort_get]];
    }];
    
    UIAlertAction * cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction * changeAction = [UIAlertAction actionWithTitle:@"修改" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        int portGet  = oneAC.textFields[0].text.intValue;
        if (portGet != 0) {
            [PnrPortEntity savePort_get:portGet];
            
            NSIndexPath * ip = [NSIndexPath indexPathForRow:2 inSection:1];
            [self.funTV reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
    
    [oneAC addAction:cancleAction];
    [oneAC addAction:changeAction];
    
    [self presentViewController:oneAC animated:YES completion:nil];
}

@end
