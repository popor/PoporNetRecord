
//
//  PnrPrefix.h
//  PoporNetRecord
//
//  Created by popor on 2019/3/11.
//  Copyright © 2019 popor. All rights reserved.
//

#ifndef PnrPrefix_h
#define PnrPrefix_h

#import "PnrEntity.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^PnrBlockPVoid) (void);
typedef void(^PnrBlockPFloatFloat) (CGFloat currentPerSecond, CGFloat maxPerSecond);
typedef void(^PnrBlockPPnrEntity) (PnrEntity * pnrEntity);
typedef void(^PnrBlockFeedback) (NSString * feedback);
typedef void(^PnrBlockResubmit) (NSDictionary * formDic, PnrBlockFeedback _Nonnull blockFeedback);

typedef NS_ENUM(NSInteger, PoporNetRecordType) {
    PoporNetRecordAuto = 1, // 开发环境或者虚拟机环境
    PoporNetRecordEnable, // 全部监测
    PoporNetRecordDisable, // 全部忽略
};

typedef NS_ENUM(NSInteger, PnrListType) {
    PnrListTypeClear,
    PnrListTypeTextColor,
    PnrListTypeTextBlack,
    PnrListTypeTextNull,
    PnrListTypeLogDetail,
    PnrListTypeLogSimply,
    PnrListTypeLogNull,
    PnrListTypeExtra,
};

NS_ASSUME_NONNULL_END

#endif /* PnrPrefix_h */
