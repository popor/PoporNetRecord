//
//  PnrWebPortEntity.m
//  PoporNetRecord
//
//  Created by popor on 2018/12/18.
//

#import "PnrWebServer.h"

#import "PnrEntity.h"
#import "PnrPortEntity.h"
#import "PnrWebBody.h"
#import "PnrConfig.h"

#import <PoporWebServer/PoporWebServer.h>
#import <PoporWebServer/PoporWebServerDataResponse.h>
#import <PoporWebServer/PoporWebServerPrivate.h>

@interface PnrWebServer ()

@property (nonatomic, weak  ) PnrConfig * config;

@property (nonatomic        ) NSInteger lastIndex;

@property (nonatomic, copy  ) NSString * h5Root;
@property (nonatomic, copy  ) NSString * h5List;
@property (nonatomic, copy  ) NSString * h5Detail;
@property (nonatomic, copy  ) NSString * h5Resubmit;

@end

@implementation PnrWebServer

+ (instancetype)share {
    static dispatch_once_t once;
    static PnrWebServer * instance;
    dispatch_once(&once, ^{
        instance = [PnrWebServer new];
        instance.h5List = [NSMutableString new];
        instance.h5Root = [PnrWebBody rootBodyIndex:0];
        
        instance.lastIndex  = -1;
        instance.config     = [PnrConfig share];
        
        // PoporWebServer 这个配置要求在主线程中执行
        dispatch_async(dispatch_get_main_queue(), ^{
            [PoporWebServer setLogLevel:kPoporWebServerLoggingLevel_Error];
        });
    });
    return instance;
}

- (id)init {
    if (self = [super init]) {
        self.portEntity = [PnrPortEntity share];
    }
    return self;
}

#pragma mark - list server
- (void)startListServer:(NSMutableString *)listBodyH5 {
    [self startListServerAsyn:listBodyH5];
}

- (NSString *)serverUrl {
    return self.webServer.serverURL.absoluteString;
}

// 异步执行, PoporWebServer初始化会和主线程冲突.
- (void)startListServerAsyn:(NSMutableString *)listBodyH5 {
    if (!listBodyH5) {
        return;
    }
    self.h5List = [PnrWebBody listH5:listBodyH5];
    __weak typeof(self) weakSelf = self;
    
    if (!self.webServer) {
        PoporWebServer * server = [PoporWebServer new];
        self.webServer = server;
        
        [self.webServer addDefaultHandlerForMethod:@"GET" requestClass:[PoporWebServerRequest class] asyncProcessBlock:^(__kindof PoporWebServerRequest * _Nonnull request, PoporWebServerCompletionBlock  _Nonnull completionBlock) {
            NSString * path = request.URL.path;
            //NSLog(@"__get path :'%@'", path);
            if (path.length >= 1) {
                path = [path substringFromIndex:1];
                NSArray * pathArray = [path componentsSeparatedByString:@"/"];
                if (pathArray.count == 2) {
                    [weakSelf analysisGetIndex:[pathArray[0] integerValue] path:pathArray[1] request:request complete:completionBlock];
                }else if (pathArray.count == 1){
                    if ([path isEqualToString:@""]) {
                        completionBlock([PoporWebServerDataResponse responseWithHTML:weakSelf.h5Root]);
                    }else if ([path isEqualToString:PnrPathList]){
                        completionBlock([PoporWebServerDataResponse responseWithHTML:weakSelf.h5List]);
                    }else if ([path isEqualToString:@"favicon.ico"]){
                        if (weakSelf.config.webIconData) {
                            completionBlock([PoporWebServerDataResponse responseWithData:weakSelf.config.webIconData contentType:@"image/x-icon"]);
                        }
                    }
                    else{
                        int index = [path intValue];
                        if ([path isEqualToString:[NSString stringWithFormat:@"%i", index]]) {
                            completionBlock([PoporWebServerDataResponse responseWithHTML:[PnrWebBody rootBodyIndex:index]]);
                        }else{
                            completionBlock([PoporWebServerDataResponse responseWithHTML:ErrorUrl]);
                        }
                    }
                }
            }
            else {
                completionBlock([PoporWebServerDataResponse responseWithHTML:ErrorUrl]);
            }
        }];
        
        [self.webServer addDefaultHandlerForMethod:@"POST" requestClass:[PoporWebServerURLEncodedFormRequest class] asyncProcessBlock:^(__kindof PoporWebServerRequest * _Nonnull request, PoporWebServerCompletionBlock  _Nonnull completionBlock) {
            NSString * path = request.URL.path;
            //NSLog(@"__post path :'%@'", path);
            if (path.length>=1) {
                path = [path substringFromIndex:1];
                NSArray * pathArray = [path componentsSeparatedByString:@"/"];
                if (pathArray.count == 1) {
                    [weakSelf analysisPost1Path:path request:request complete:completionBlock];
                }
                //else if (pathArray.count == 2) {
                //    [weakSelf analysisPost2Index:[pathArray[0] integerValue] path:pathArray[1] request:request complete:completionBlock];
                //}
                else {
                    completionBlock([PoporWebServerDataResponse responseWithHTML:ErrorUrl]);
                }
            }
            else{
                completionBlock([PoporWebServerDataResponse responseWithHTML:ErrorUrl]);
            }
        }];
        
        PnrPortEntity * port = [PnrPortEntity share];
        [server startWithPort:port.portGetInt bonjourName:nil];
    }
}

// 分析 get 请求
- (void)analysisGetIndex:(NSInteger)index path:(NSString *)path request:(PoporWebServerRequest * _Nonnull)request complete:(PoporWebServerCompletionBlock  _Nonnull)complete
{
    PnrEntity * entity;
    if (self.infoArray.count > index) {
        entity = self.infoArray[index];
    }
    if (entity) {
        if (index != self.lastIndex) {
            self.lastIndex = index;
            [self startServerUnitEntity:entity index:index];
        }
        NSString * str;
        if ([path isEqualToString:PnrPathList]) {
            str = self.h5List;
        }else if ([path isEqualToString:PnrPathDetail]) {
            str = self.h5Detail;
        }else if([path isEqualToString:PnrPathEdit]){
            str = self.h5Resubmit;
        }
        if (str) {
            complete([PoporWebServerDataResponse responseWithHTML:str]);
        }else{
            complete([PoporWebServerDataResponse responseWithHTML:ErrorUnknow]);
        }
        
    }else{
        complete([PoporWebServerDataResponse responseWithHTML:ErrorEntity]);
    }
}

- (void)analysisPost1Path:(NSString *)path request:(PoporWebServerRequest * _Nonnull)request complete:(PoporWebServerCompletionBlock  _Nonnull)complete {
    
    PoporWebServerURLEncodedFormRequest * formRequest = (PoporWebServerURLEncodedFormRequest *)request;
    NSDictionary * dic = formRequest.arguments;
    if ([path isEqualToString:PnrPathJsonXml]) {
        NSString * str = dic[PnrKeyConent];
        if (str) {
            complete([PoporWebServerDataResponse responseWithHTML:dic[PnrKeyConent]]);
        }else{
            complete([PoporWebServerDataResponse responseWithHTML:ErrorEmpty]);
        }
    }
    else if([path isEqualToString:PnrPathResubmit]){
        if (self.resubmitBlock) {
            PnrBlockFeedback blockFeedback ;
            blockFeedback = ^(NSString * feedback) {
                if (!feedback) {
                    feedback = @"NULL";
                }
                complete([PoporWebServerDataResponse responseWithHTML:feedback]);
            };
            PoporWebServerURLEncodedFormRequest * formRequest= (PoporWebServerURLEncodedFormRequest *)request;
            self.resubmitBlock(formRequest.arguments, blockFeedback);
        }else{
            complete([PoporWebServerDataResponse responseWithHTML:ErrorResubmit]);
        }
    }
    else if([path isEqualToString:PnrPathClear]){
        [self.infoArray removeAllObjects];
        [self clearListWeb];
        
        complete([PoporWebServerDataResponse responseWithHTML:@"clear finish"]);
    }
    else if ([path isEqualToString:@"favicon.ico"]){
        if (self.config.webIconData) {
            complete([PoporWebServerDataResponse responseWithData:self.config.webIconData contentType:@"image/x-icon"]);
        }
    }
    
    else{
        complete([PoporWebServerDataResponse responseWithHTML:ErrorUrl]);
    }
}

#pragma mark - server 某个单独请求
- (void)startServerUnitEntity:(PnrEntity *)pnrEntity index:(NSInteger)index {
    [PnrWebBody deatilEntity:pnrEntity index:index extra:self.resubmitExtraDic finish:^(NSString * _Nonnull detail, NSString * _Nonnull resubmit) {
        self.h5Detail   = detail;
        self.h5Resubmit = resubmit;
    }];
}

- (void)stopServer {
    [self.webServer stop];
    self.webServer = nil;
}

- (void)clearListWeb {
    self.lastIndex = -1;
    self.h5List    = [PnrWebBody listH5:@""];
}

@end


// MARK: 分析 post 多层
//- (void)analysisPost2Index:(NSInteger)index path:(NSString *)path request:(PoporWebServerRequest * _Nonnull)request complete:(PoporWebServerCompletionBlock  _Nonnull)complete {
//
//    PnrEntity * entity;
//    if (self.infoArray.count > index) {
//        entity = self.infoArray[index];
//    }
//    if (entity) {
//        if (index != self.lastIndex) {
//            self.lastIndex = index;
//            [self startServerUnitEntity:entity index:index];
//        }
//        if([path isEqualToString:PnrPathResubmit]){
//            if (self.resubmitBlock) {
//                PnrBlockFeedback blockFeedback ;
//                blockFeedback = ^(NSString * feedback) {
//                    if (!feedback) {
//                        feedback = @"NULL";
//                    }
//                    complete([PoporWebServerDataResponse responseWithHTML:feedback]);
//                };
//                PoporWebServerURLEncodedFormRequest * formRequest= (PoporWebServerURLEncodedFormRequest *)request;
//                self.resubmitBlock(formRequest.arguments, blockFeedback);
//            }else{
//                complete([PoporWebServerDataResponse responseWithHTML:ErrorResubmit]);
//            }
//        }else{
//            complete([PoporWebServerDataResponse responseWithHTML:ErrorUrl]);
//        }
//
//    }else{
//        complete([PoporWebServerDataResponse responseWithHTML:ErrorEntity]);
//    }
//}
